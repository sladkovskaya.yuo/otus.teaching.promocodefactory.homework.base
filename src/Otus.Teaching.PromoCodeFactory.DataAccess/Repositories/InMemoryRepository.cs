﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public bool CreateAsync(T obj)
        {
            try
            {
                Data = Data.Append(obj);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteAsync(Guid id)
        {
            try
            {
                var deletePerson = Data.Where(x => x.Id != id);
                if (Data.Count() - deletePerson.Count() != 1)
                {
                    return false;
                }
                Data = deletePerson;
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool UpdateAsync(Guid id, T obj)
        {
            try
            {
                var exemplar = Data.FirstOrDefault(x => x.Id == id);
                if (exemplar == null)
                {
                    return false;
                }
                exemplar = obj;
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}