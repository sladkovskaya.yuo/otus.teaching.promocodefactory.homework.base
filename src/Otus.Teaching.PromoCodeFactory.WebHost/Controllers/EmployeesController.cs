﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roles;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roles)
        {
            _employeeRepository = employeeRepository;
            _roles = roles;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<bool>> CreateEmployeesAsync(CreateEmployeeRequest employeeInfo)
        {
            // при создании присваивается роль PartnerManager
            var role = await _roles?.GetByIdAsync(Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"));
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = employeeInfo.FirstName,
                LastName = employeeInfo.LastName,
                Email = employeeInfo.Email,
                Roles = new List<Role>()
                {
                    role
                }
            };
            var res = _employeeRepository.CreateAsync(employee);
  
            return res;
        }

        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public ActionResult<bool> DeleteById(Guid id) =>
                            _employeeRepository.DeleteAsync(id);

        /// <summary>
        /// Изменить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<bool>> UpdateEmployeesAsync(Guid id, CreateEmployeeRequest employeeInfo)
        {
            var record = await _employeeRepository.GetByIdAsync(id);
            record.FirstName = employeeInfo.FirstName;
            record.LastName = employeeInfo.LastName;
            record.Email = employeeInfo.Email;
            return _employeeRepository.UpdateAsync(id, record);
        }
    }
}